package com.service;

import org.springframework.stereotype.Service;

import com.model.SubmitConfig;

@Service
public interface SummarizationService {

	public void summarizeAsyncWrapper(SubmitConfig subCfg, String email) throws Exception;
	public void summarize(SubmitConfig subCfg, String email) throws Exception ;
	
}
