package com.test.system;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.start.abstat.AbstatApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class AutocompleteAPITest {
	
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnFields() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "m")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("subj",  "http://schema.org/Place"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"prefix\":", 2)))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\":", 2)))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"namespace\":", 2)))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"occurrence\":", 2)))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"dataset\":", 2)));
    }
    @Test
    public void shortQStringShouldReturnEmptyListIfNotMaches() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "xj")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("subj",  "http://schema.org/Place"))
                .andExpect(content().string(containsString("{\"suggestions\":[]}")));
    }
    @Test
    public void qStringShouldReturnEmptyListIfNotMaches() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "xjkl")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("subj",  "http://schema.org/Place"))
                .andExpect(content().string(containsString("{\"suggestions\":[]}")));
    }
    
    /*################################################ MONO-DATASET ################################################*/
    
    
    //------------------------------------------ CHECK AKPs IDEXING ------------------------------------------------------*/
    
    @Test
    public void objectAKPsShouldBeIndexed() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "office")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("pred",  "http://xmlns.com/foaf/0.1/name")
        		.param("obj",  "http://www.w3.org/2000/01/rdf-schema#Literal"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 1)));
    }
    
    @Test
    public void datatypeAKPsShouldBeIndexed() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "date")
        		.param("qPosition", "obj")
        		.param("dataset", "system-test")
        		.param("subj",  "http://wikidata.dbpedia.org/resource/Q5")
        		.param("pred",  "http://dbpedia.org/ontology/birthDate"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 1)));
    }
    
    //-------------------------------------- CHECK QSTRING & QPOSITION & CONSTRAINTS --------------------------------------------*/
    
    @Test
    public void shortqStringAndSubjConstraintShouldWorkOnPred() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "m")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("subj",  "http://schema.org/Place"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("m")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 2)));
    }
    @Test
    public void shortqStringAndSubjConstraintShouldWorkOnObj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "l")
        		.param("qPosition", "obj")
        		.param("dataset", "system-test")
        		.param("subj",  "http://schema.org/Place"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("l")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 1)));
    }
    @Test
    public void qStringAndSubjConstraintShouldWorkOnPred() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "ele")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("subj",  "http://schema.org/Place"))
                .andExpect(content().string(SuggestionMatcher.containsQString("ele")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 2)));
    }
    @Test
    public void qStringAndSubjConstraintShouldWorkOnObj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "kilo")
        		.param("qPosition", "obj")
        		.param("dataset", "system-test")
        		.param("subj",  "http://schema.org/Place"))
                .andExpect(content().string(SuggestionMatcher.containsQString("kilo")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 2)));
    }
    
 //------
    
    @Test
    public void shortqStringAndPredConstraintShouldWorkOnSubj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "p")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("pred",  "http://dbpedia.org/ontology/birthPlace"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("p")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 12)));
    }
    @Test
    public void shortqStringAndPredConstraintShouldWorkOnObj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "a")
        		.param("qPosition", "obj")
        		.param("dataset", "system-test")
        		.param("pred",  "http://dbpedia.org/ontology/birthPlace"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("a")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 14)));
    }
    
    @Test
    public void qStringAndPredConstraintShouldWorkOnSubj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "person")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("pred",  "http://dbpedia.org/ontology/birthPlace"))
                .andExpect(content().string(SuggestionMatcher.containsQString("person")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 18)));
    }
    @Test
    public void qStringAndPredConstraintShouldWorkOnObj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "area")
        		.param("qPosition", "obj")
        		.param("dataset", "system-test")
        		.param("pred",  "http://dbpedia.org/ontology/birthPlace"))
                .andExpect(content().string(SuggestionMatcher.containsQString("area")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 7)));
    }
    
 //------
    
    @Test
    public void shortqStringAndObjConstraintShouldWorkOnSubj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "s")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("obj",  "http://dbpedia.org/datatype/squareKilometre"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("s")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 1)));
    }
    @Test
    public void shortqStringAndObjConstraintShouldWorkOnPred() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "a")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("obj",  "http://dbpedia.org/datatype/squareKilometre"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("a")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 8)));
    }
    @Test
    public void qStringAndObjConstraintShouldWorkOnSubj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "reg")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("obj",  "http://dbpedia.org/datatype/squareKilometre"))
                .andExpect(content().string(SuggestionMatcher.containsQString("reg")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 1)));
    }
    @Test
    public void qStringAndObjConstraintShouldWorkOnPred() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "total")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("obj",  "http://dbpedia.org/datatype/squareKilometre"))
                .andExpect(content().string(SuggestionMatcher.containsQString("total")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 8)));
    }
    
    
 /*###################################### CHECK PAGING #########################################################*/
    
    @Test
    public void shouldWorkWithValidLRows() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "is")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("rows", "2")
        		.param("subj",  "http://schema.org/Place"))
				.andExpect(content().string(SuggestionMatcher.startWirhQString("is")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 2)));
    }
    @Test
    public void shouldWorkWithInvalidLRows() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "ispart")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("rows", "-3")
        		.param("subj",  "http://schema.org/Place"))
				.andExpect(content().string(SuggestionMatcher.startWirhQString("is")))
				.andExpect(content().string(containsString("{\"suggestions\":[]}")));
    }
    
 //-----
    
    @Test
    public void shouldWorkWithValidLStart() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "administrative")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("obj",  "http://dbpedia.org/datatype/squareKilometre")
        		.param("start", "1"))
        .andExpect(content().string(SuggestionMatcher.containsQString("administrative")))
        .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 1)));
    }
    @Test
    public void shouldWorkWithOverflowStart() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "administrative")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("obj",  "http://dbpedia.org/datatype/squareKilometre")
        		.param("start", "100"))
		.andExpect(content().string(containsString("{\"suggestions\":[]}")));
    }
    
 //-----
    
    @Test
    public void pagingSholdWork() throws Exception{
    	 mockMvc.perform(get("/api/v1/SolrSuggestions")
         		.param("qString", "person")
         		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("pred", "http://dbpedia.org/ontology/birthPlace")
        		.param("group", "true")
    			.param("start", "0")
    			.param("rows", "2"))
    	 .andExpect(content().string(containsString("http:\\/\\/schema.org\\/Person")))
    	 .andExpect(content().string(containsString("http:\\/\\/www.ontologydesignpatterns.org\\/ont\\/dul\\/DUL.owl#NaturalPerson")));
    }
    @Test
    public void pagingSholdWork2() throws Exception{
    	 mockMvc.perform(get("/api/v1/SolrSuggestions")
         		.param("qString", "person")
         		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("pred", "http://dbpedia.org/ontology/birthPlace")
        		.param("group", "true")
    			.param("start", "1")
    			.param("rows", "2"))
    	 .andExpect(content().string(not(containsString("http:\\/\\/schema.org\\/Person"))))
    	 .andExpect(content().string(containsString("http:\\/\\/www.ontologydesignpatterns.org\\/ont\\/dul\\/DUL.owl#NaturalPerson")));
    }
    @Test
    public void pagingSholdWork3() throws Exception{
    	 mockMvc.perform(get("/api/v1/SolrSuggestions")
         		.param("qString", "person")
         		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("pred", "http://dbpedia.org/ontology/birthPlace")
        		.param("group", "true")
    			.param("start", "2")
    			.param("rows", "3"))
    	 .andExpect(content().string(not(containsString("http:\\/\\/schema.org\\/Person"))))
    	 .andExpect(content().string(not(containsString("http:\\/\\/www.ontologydesignpatterns.org\\/ont\\/dul\\/DUL.owl#NaturalPerson"))))
    	 .andExpect(content().string(containsString("http:\\/\\/xmlns.com\\/foaf\\/0.1\\/Person")));
    }
    @Test
    public void pagingSholdWork4() throws Exception{
    	 mockMvc.perform(get("/api/v1/SolrSuggestions")
         		.param("qString", "person")
         		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("pred", "http://dbpedia.org/ontology/birthPlace")
        		.param("group", "true")
    			.param("start", "0")
    			.param("rows", "100"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 3)));
    }

    
 /*###################################### CHECK GROUP #########################################################*/

    
    @Test
    public void shouldWorkWithResourcesWithSameLabelButDifferentPrefix() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "p")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test")
        		.param("pred",  "http://dbpedia.org/ontology/birthPlace")
        		.param("group",  "true"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("p")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 2)));
    }
   
   
 //------------------------------------------ CHECK RANKING ------------------------------------------------------*/
    
    @Test
    public void shouldBeRankDescOnFrequency() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		list.add("\"occurrence\":2");
		list.add("\"occurrence\":1");

    	mockMvc.perform(get("/api/v1/SolrSuggestions")
    			.param("qString", "birth")
         		.param("qPosition", "pred")
         		.param("subj", "http://schema.org/Person")
        		.param("dataset", "system-test")
        		.param("group", "true"))
    	 .andExpect(content().string(stringContainsInOrder(list)));
    }
    
}
