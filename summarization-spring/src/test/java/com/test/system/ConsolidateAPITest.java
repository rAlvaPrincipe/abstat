package com.test.system;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.start.abstat.AbstatApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class ConsolidateAPITest {

	  @Autowired
	  private MockMvc mockMvc;
	  
	  @Test
	  public void shouldWork() throws Exception {
		  this.mockMvc.perform(post("/consolidate")
	        		.param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
	        		.param("store", "true")
		  			.param("index", "true")
		  			.param("domain", "dbpedia.org"))
	        .andDo(print()).andExpect(status().is3xxRedirection());
	  }
	  
}
