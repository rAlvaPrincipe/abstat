package com.test.system;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import java.io.File;
import java.io.FileOutputStream;

import org.hamcrest.BaseMatcher;

 
public class SuggestionMatcher {
	public static Matcher<String> startWirhQString(final String s) {
		return new BaseMatcher<String>() {
 
			@Override
			public void describeTo(final Description description) {
				description.appendText("a string with suggestions containing "+s);
				
			}
			 
			@Override
			public boolean matches(final Object item) {
				String input = (String)item;				
				boolean isCorrect = true;
				String st = "\"suggestion\":\"";
				
				while(input.contains("\"suggestion\":")){
					int index = input.indexOf(st);
					input = input.substring(index + st.length());
					String suggestion = input.substring(0, input.indexOf("\",\""));
					
					String sugLabel = "";
					if(suggestion.lastIndexOf("/") > suggestion.lastIndexOf("#"))
						sugLabel = suggestion.substring(suggestion.lastIndexOf("/")+1);
					else
						sugLabel = suggestion.substring(suggestion.lastIndexOf("#")+1);
					try {
					FileOutputStream fos = new FileOutputStream(new File("/home/renzo/test.txt"), true);
					fos.write((suggestion+"\n"+sugLabel+"\n").getBytes());
					}
					catch(Exception e) {}
					if(!sugLabel.toLowerCase().startsWith(s))
						isCorrect = false;	
				}
				
				return isCorrect;
			}
 
			@Override
			public void describeMismatch(final Object item, final Description mismatchDescription) {
				mismatchDescription.appendText("was ").appendText((String)item);
			}
			
		};
	}
	
	
	public static Matcher<String> containsQString(final String s) {
		return new BaseMatcher<String>() {
 
			@Override
			public void describeTo(final Description description) {
				description.appendText("a string with suggestions containing "+s);
				
			}
			 
			@Override
			public boolean matches(final Object item) {
				String input = (String)item;				
				boolean isCorrect = true;
				String st = "\"suggestion\":\"";
				
				while(input.contains("\"suggestion\":")){
					int index = input.indexOf(st);
					input = input.substring(index + st.length());
					String suggestion = input.substring(0, input.indexOf("\",\""));
					
					String sugLabel = "";
					if(suggestion.lastIndexOf("/") > suggestion.lastIndexOf("#"))
						sugLabel = suggestion.substring(suggestion.lastIndexOf("/")+1);
					else
						sugLabel = suggestion.substring(suggestion.lastIndexOf("#")+1);
					try {
					FileOutputStream fos = new FileOutputStream(new File("/home/renzo/test.txt"), true);
					fos.write((suggestion+"\n"+sugLabel+"\n").getBytes());
					}
					catch(Exception e) {}
					if(!sugLabel.toLowerCase().contains(s))
						isCorrect = false;	
				}
				
				return isCorrect;
			}
 
			@Override
			public void describeMismatch(final Object item, final Description mismatchDescription) {
				mismatchDescription.appendText("was ").appendText((String)item);
			}
			
		};
	}
}