package com.test.system;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.start.abstat.AbstatApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class SPOAPITest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldWorkWithSubject() throws Exception {
        this.mockMvc.perform(get("/api/v1/SPO")
        		.param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
        		.param("position", "subject"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"subject\"", 16)));
    }
    
    @Test
    public void shouldWorkWithPredicate() throws Exception {
        this.mockMvc.perform(get("/api/v1/SPO")
        		.param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
        		.param("position", "predicate"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"predicate\"", 16)));
    }
    
    @Test
    public void shouldWorkWithObject() throws Exception {
        this.mockMvc.perform(get("/api/v1/SPO")
        		.param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
        		.param("position", "object"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"object\"", 16)));
    }
    
    
    //TO-DO 
    //load a second summary and write the same tests without "summary" parameter
    
}
