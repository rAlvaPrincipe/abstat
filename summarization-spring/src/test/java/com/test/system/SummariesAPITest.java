package com.test.system;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.start.abstat.AbstatApplication;




@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class SummariesAPITest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldWork() throws Exception {
        this.mockMvc.perform(get("/api/v1/summaries")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"summaries\":")));
    }
    
    @Test
    public void shouldReturnAllSummaries() throws Exception {
        this.mockMvc.perform(get("/api/v1/summaries")
        		.param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x"))
                .andExpect(content().string(containsString("a7ad60fb-060c-48ad-844b-a768ebb7e79x")));
    }    
    
    @Test
    public void shouldReturnMatchingSummaries1() throws Exception {
        this.mockMvc.perform(get("/api/v1/summaries")
        		.param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
        		.param("indexed", "true")
        		.param("loaded", "true"))
                .andExpect(content().string(containsString("a7ad60fb-060c-48ad-844b-a768ebb7e79x")));
    }
    
    @Test
    public void shouldReturnMatchingSummaries2() throws Exception {
        this.mockMvc.perform(get("/api/v1/summaries")
        		.param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
        		.param("indexed", "false")
        		.param("loaded", "false"))
                .andExpect(content().string(not(containsString("a7ad60fb-060c-48ad-844b-a768ebb7e79x"))));
    } 
}
