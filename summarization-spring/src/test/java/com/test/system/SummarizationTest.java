package com.test.system;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.model.SubmitConfig;
import com.service.SummarizationService;
import com.start.abstat.AbstatApplication;
import org.junit.Assert; 

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
public class SummarizationTest {

	@Autowired
	SummarizationService summarizationServ;
	  
	@Test
	public void summarize() throws Exception{
		ArrayList<String> listOntId = new ArrayList<String>();
		listOntId.add("system-test_ontology");
		ArrayList<String> listOntNames = new ArrayList<String>();
		listOntNames.add("dbpedia_2014");
		
		Date dNow = new Date( );
		SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
		    
		SubmitConfig conf = new SubmitConfig();
		conf.setId("a7ad60fb-060c-48ad-844b-a768ebb7e79x");
		conf.setDsId("system-test_dataset");
		conf.setDsName("system-test");
		conf.setListOntId(listOntId);
		conf.setListOntNames(listOntNames);
		conf.setSummaryPath("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInf/");
		conf.setTimestamp(ft.format(dNow));
		conf.setTipoMinimo(true);
		conf.setPropertyMinimaliz(true);
		conf.setInferences(true);
		conf.setCardinalita(true);	
		summarizationServ.summarize(conf, null);
		
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/count-concepts.txt").exists());
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/count-datatype.txt").exists());
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/count-datatype-properties.txt").exists());
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/count-object-properties.txt").exists());
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/datatype-akp.txt").exists());
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/datatype-akp_grezzo.txt").exists());
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/object-akp.txt").exists());
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/object-akp_grezzo.txt").exists());
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/patterns_splitMode_datatype.txt").exists());
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/patterns_splitMode_object.txt").exists());
		Assert.assertTrue(new File(conf.getSummaryPath() + "patterns/patternCardinalities.txt").exists());
	  }
}
