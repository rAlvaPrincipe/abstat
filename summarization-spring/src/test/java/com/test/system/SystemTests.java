package com.test.system;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import com.start.abstat.AbstatApplication;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	SummarizationTest.class,
	ConsolidateAPITest.class,
    PatternGraphTest.class,
    CardinalitiesTest.class,
    PropertiesMinimalizationTest.class,
    BrowseAPITest.class,
    SPOAPITest.class,
    AutocompleteAPINoConstraintTest.class,
    AutocompleteAPITest.class
    
})
@ContextConfiguration(classes = AbstatApplication.class)
public class SystemTests {}

