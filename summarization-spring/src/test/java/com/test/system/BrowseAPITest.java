package com.test.system;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.start.abstat.AbstatApplication;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class BrowseAPITest {

    @Autowired
    private MockMvc mockMvc;

    
    @Test
    public void shouldWorkWithNoParams() throws Exception {
    	mockMvc.perform(get("/api/v1/browse")).andDo(print()).andExpect(status().isOk())
    	.andExpect(content().string(containsString("{ \"akps\": [ ")));
    }
    
    
    @Test
    public void shouldWork() throws Exception {
        mockMvc.perform(get("/api/v1/browse")
        		.param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x"))
        .andDo(print()).andExpect(status().isOk())
        .andExpect(content().string(containsString("{ \"akps\": [ ")));
    }
    
    /*------------------------------------------- output fields -----------------------------------------------*/
    
    @Test
    public void shouldShowSubjectsPredicatesObjects() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"subject\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"predicate\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"object\" :", 178)));
    }
    
    @Test
    public void shouldShowAKPTypes() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"type\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"subType\" :", 178)));
    }
    
    @Test
    public void shouldShowMetadata() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"datasetOfOrigin\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"ontologiesOfOrigin\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"summary_conf\" :", 178)));
    }
    
    @Test
    public void shouldShowStatistics() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"frequency\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"numberOfInstances\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"cardinality1\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"cardinality2\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"cardinality3\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"cardinality4\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"cardinality5\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("cardinality6\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("cardinality6", 178)));
    }
    
    
    /*------------------------------------------- with constraints -----------------------------------------------*/
    
    @Test
    public void shouldWorkWithInternalSubject() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("\"subject\" : \"http://schema.org/Place\",", 18)));
    }
    
    @Test
    public void shouldWorkWithExternalSubject() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://www.opengis.net/gml/_Feature"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("\"subject\" : \"http://www.opengis.net/gml/_Feature\",", 9)));
    }
    
    @Test
    public void shouldWorkWithPredicate() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("pred", "http://xmlns.com/foaf/0.1/name"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("\"predicate\" : \"http://xmlns.com/foaf/0.1/name\",", 22)));
    }
    
    @Test
    public void shouldWorkWithObject() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("obj", "http://www.w3.org/2001/XMLSchema#double"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("\"object\" : \"http://www.w3.org/2001/XMLSchema#double\",", 26)));
    }
    
    @Test
    public void shouldWorkWithSubjectAndPredicate() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place")
    			 .param("pred", "http://dbpedia.org/ontology/isPartOf"))
    	 .andExpect(content().string(containsString("05c52090bf31df605a6bd9ea114a676d")))
    	 .andExpect(content().string(containsString("8d3c770d0b6f99929625a4781ff02820")))
    	 .andExpect(content().string(containsString("b6944165f604af81e0b43cc4f4b85cd1")))
    	 .andExpect(content().string(containsString("258f1c471f1f0c23c8035435d5f2bce2")));
    }
    
    @Test
    public void shouldWorkWithSubjectAndObject() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place")
    			 .param("obj", "http://www.w3.org/2001/XMLSchema#double"))
    	 .andExpect(content().string(containsString("82242e5e32bc2d53e51f9f3309c17717")))
    	 .andExpect(content().string(containsString("a40f666c88ec5a75ae5361bd22b1063b")))
    	 .andExpect(content().string(containsString("38e5da578fbd0c69eba6ceb79f08798e")))
    	 .andExpect(content().string(containsString("523f684e9e8d2f74584a8d80a72c7ccd")))
    	 .andExpect(content().string(containsString("705a0d4368401f494020f32045c4cd88")));
    }
    
    @Test
    public void shouldWorkWithpredicateAndObject() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("pred", "http://dbpedia.org/ontology/locationCity")
    			 .param("obj", "http://dbpedia.org/ontology/Place"))
    	 .andExpect(content().string(containsString("2ecd2f6f378a388d6dfcd1ac4d39fda3")));
    }
    
    /*--------------------- check SPO enrichment -----------------------------*/
    
    @Test
    public void should() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place")
    			 .param("pred", "http://dbpedia.org/ontology/isPartOf")
    			 .param("enrichWithSPO", "true"))
    	 .andExpect(content().string(containsString("  \"subject\" : {\n" + 
    	 		"    \"globalURL\" : \"http://schema.org/Place\",\n" + 
    	 		"    \"frequency\" : 3")))
    	 .andExpect(content().string(containsString(" \"predicate\" : {\n" + 
    	 		"    \"globalURL\" : \"http://dbpedia.org/ontology/isPartOf\",\n" + 
    	 		"    \"frequency\" : 1")))
    	 .andExpect(content().string(containsString("\"object\" : {\n" + 
    	 		"    \"globalURL\" : \"http://dbpedia.org/ontology/AdministrativeRegion\",\n" + 
    	 		"    \"frequency\" : 1")))
    	 .andExpect(content().string(not(containsString("\"subject\" : \""))))
    	 .andExpect(content().string(not(containsString("\"predicate\" : \""))))
    	 .andExpect(content().string(not(containsString("\"object\" : \""))));
    }
    
    /*--------------------- check ranking -----------------------------*/
    
    @Test
    public void shouldRankInDescOrder() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		list.add(" \"frequency\" : 6,");
		list.add(" \"frequency\" : 3,");
		list.add(" \"frequency\" : 2,");
		list.add(" \"frequency\" : 2,");
		list.add(" \"frequency\" : 2,");
		list.add(" \"frequency\" : 1,");
		
    	mockMvc.perform(get("/api/v1/browse")
    			.param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			.param("subj", "http://schema.org/Place"))
    	 .andExpect(content().string(stringContainsInOrder(list)));
    }
    

    /*--------------------- check paging -----------------------------*/
    
    @Test
    public void shouldWorkWithValidLimit() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place")
    			 .param("limit", "10"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"subject\" :", 10)));
    }
    
    @Test
    public void shouldWReturnAllResultsWithInvalidLimit() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place")
    			 .param("limit", "-2"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"subject\" :", 18)));
    }
    
    @Test
    public void shouldWReturnAllResultsWithLimitZero() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place")
    			 .param("limit", "0"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"subject\" :", 18)));
    }
    
    //----
    
    @Test
    public void shouldWorkWithValidOffset() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place")
    			 .param("offset", "3"))
    	 .andExpect(content().string(not(containsString("dbfdba22fe2f2234f0b4ffea3eea86ae"))))
    	 .andExpect(content().string(not(containsString("ae87df565d5e8fd29ad9f809d8200648"))))
    	 .andExpect(content().string(not(containsString("82242e5e32bc2d53e51f9f3309c17717"))));
    }
    
    //----
    
    @Test
    public void pagingSholdWorkWithNormalParams1() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place")
    			 .param("offset", "0")
    			 .param("limit", "3"))
    	 .andExpect(content().string(containsString("dbfdba22fe2f2234f0b4ffea3eea86ae")))
    	 .andExpect(content().string(containsString("ae87df565d5e8fd29ad9f809d8200648")))
    	 .andExpect(content().string(containsString("82242e5e32bc2d53e51f9f3309c17717")));
    }
    
    @Test
    public void pagingSholdWorkWithNormalParams2() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place")
    			 .param("offset", "2")
    			 .param("limit", "3"))
    	 .andExpect(content().string(containsString("82242e5e32bc2d53e51f9f3309c17717")))
    	 .andExpect(content().string(containsString("a40f666c88ec5a75ae5361bd22b1063b")))
    	 .andExpect(content().string(containsString("38e5da578fbd0c69eba6ceb79f08798e")));
    }
    
    @Test
    public void pagingSholdWorkWithOverflowParams() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://schema.org/Place")
    			 .param("offset", "16")
    			 .param("limit", "4"))
    	 .andExpect(content().string(containsString("efba4f763f386a4458b2327f03de3b7d")))
    	 .andExpect(content().string(containsString("c9b42d49b341b2c39711d7b31d62498f")));
    }
    
    
    /*--------------------- check everything -----------------------------*/
    
    @Test
    public void shouldWorkEverything1() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#Agent")
    			 .param("pred", "http://dbpedia.org/ontology/birthPlace")
    			 .param("offset", "2")
    			 .param("limit", "10"))
    	 .andExpect(content().string(containsString("21b82ff123dfed74e5e916184d6b11a2")))
    	 .andExpect(content().string(containsString("5d56bb9c89657b453d045fadfe72195a")))
    	 .andExpect(content().string(containsString("8114384b9ede292e75d222b7cbdfbca3")));
    }
    
    @Test
    public void shouldWorkEverything2() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		list.add("a7ca1cee3988626f0e4abe4d85055fd5");
		list.add("0b33a1d726594089bda0c528bc048406");
		list.add("5cbdb44ce4a1568831cffc3ed2dae934");
		list.add("365764e40b5781e2dc5b8f52fe914599");
		list.add("81eac743828f9537cc8bdd5fea9b71e8");
		
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", "a7ad60fb-060c-48ad-844b-a768ebb7e79x")
    			 .param("subj", "http://dbpedia.org/ontology/Wikidata:Q532")
    			 .param("enrichWithSPO", "true")
    			 .param("offset", "1")
    			 .param("limit", "5"))
    	 .andExpect(content().string(not(containsString("9c71c94af99d616ed70d03c26db613d9"))))
    	 .andExpect(content().string(stringContainsInOrder(list)))
    	 .andExpect(content().string(not(containsString("\"subject\" : \""))))
    	 .andExpect(content().string(not(containsString("\"predicate\" : \""))))
    	 .andExpect(content().string(not(containsString("\"object\" : \""))))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("  \"subject\" : {\n    \"globalURL\" :",5)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("  \"predicate\" : {\n    \"globalURL\" :",5)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("  \"object\" : {\n    \"globalURL\" :",5)));
    }
    
    
}
